### Hi there 👋

<!--
**AshleyDumaine/AshleyDumaine** is a ✨ _special_ ✨ repository because its `README.md` (this file) appears on your GitHub profile.

Here are some ideas to get you started:

- 🔭 I’m currently working on ...
- 🌱 I’m currently learning ...
- 👯 I’m looking to collaborate on ...
- 🤔 I’m looking for help with ...
- 💬 Ask me about ...
- 📫 How to reach me: ...
- 😄 Pronouns: ...
- ⚡ Fun fact: ...
-->

I am a Senior Software Engineer at [Akamai](https://akamai.com) / [Linode](https://linode.com).

🔭 I’m currently working with / on:
- :computer: Go, Python, Bash
- :cloud: Kubernetes, Serverless / FaaS, GitOps, Observability (monitoring, alerting, and logging), Ansible, and Terraform

🌱 I’m currently learning:
- :technologist: all things serverless / FaaS
- :thinking: probably yet another new CNCF project

⚡ Fun facts:
- :green_book: I sometimes blog on [chowner.com](https://chowner.com/blog)
- I like anime, :curry: curry, :cat: cats, and playing Beat Saber

😄 Pronouns: she/her
